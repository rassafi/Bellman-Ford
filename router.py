import socket
import threading
from time import sleep
import sys
from pynput import keyboard
import numpy as np
#from colorclass import Color, Windows
from terminaltables import SingleTable




class Router():
    def __init__(self, portfile, costfile, rid, ip='127.0.0.1'):
        self.__cost_updated = False
        self.__num = int(rid)-1
        self.__costfile = costfile
        self.__PORTS = list()
        self.__UDP_IP = ip
        self.__read_ports(portfile)
        self.__adj_mat = np.full((int(len(self.__PORTS)), int(len(self.__PORTS))), np.inf)
        self.__neighborhood = [False]*len(self.__PORTS)
        self.__routTable = [-1]*len(self.__PORTS) # uses ID, not __num
        self.__read_cost()
        self.__PORT = self.__PORTS[self.__num]
        self.__send_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.__rcv_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.__rcv_sock.bind((self.__UDP_IP,self.__PORT))
        self.__udp_rcv_T = threading.Thread(target=self.__udp_rcv)
        self.__udp_rcv_T.daemon = True
        self.__udp_rcv_T.start()

        print("\n listen on PORT = ",  str(self.__PORT), " and IP = ", str(ip), "\n")

    def start_route(self):
        self.__update_table_notify()

    def check_link_cost(self):
        with open(self.__costfile) as f:
            cost = f.read().split()

        dcost = [int(d) if d!='N' else np.inf for d in cost]
        npcost = np.array(dcost).reshape(int(len(self.__PORTS)), int(len(self.__PORTS)))

        if not np.array_equal(npcost[self.__num, :], self.__adj_mat[self.__num, :]):  # change in neighbor link costs
            self.__adj_mat[self.__num, :] = npcost[self.__num, :].copy()
            self.__cost_updated = True
            __update_table_notify()

    def __read_ports(self, port_file):
        with open(port_file) as f:
            port_list = f.read().split()

        if len(port_list)%2 == 0:
            self.__PORTS = list(map(int, port_list[1::2]))

    def get_port(self):
        return self.__PORT

    def __read_cost(self):
        # reading and parsing the file
        with open(self.__costfile) as f:
            cost = f.read().split()

        dcost = [int(d) if d!='N' else np.inf for d in cost]
        npcost = np.array(dcost).reshape(int(len(self.__PORTS)), int(len(self.__PORTS)))
        # set the value of this router

        self.__adj_mat[self.__num, :] = npcost[self.__num, :].copy()
        self.__neighborhood = [True if n!=0 and n!=np.inf else False for n in npcost[self.__num, :]]
        self.__routTable = [r+1 if nei else -1 for (r, nei) in zip(range(0, len(self.__neighborhood)), self.__neighborhood)]
        self.__cost_updated = True

    def __update_table_notify(self):
        if self.__cost_updated:
            self.__cost_updated = False
            for router in range(len(self.__PORTS)):
                cost = self.__adj_mat[self.__num,router]
                for (j, nei) in zip(range(len(self.__PORTS)), self.__neighborhood):
                    if nei:
                        dcost = self.__adj_mat[self.__num,j]+self.__adj_mat[j,router]
                        if dcost < cost:
                            self.__adj_mat[self.__num, router] = dcost
                            self.__routTable[router] = j + 1;
                            # self.__cost_updated = True
            self.show_table()
            # self.udp_send_neighbors(str(self.__adj_mat[self.__num,:].tolist())+"-"+str(self.__num+1))
            self.udp_send_neighbors_poison()
            #print(self.__routTable)

    def show_table(self):
        #table_instance = SingleTable(self.__adj_mat.tolist(),Color('{autocyan}'+'router id = '+str(self.__num+1)+'{/autocyan}'))
        table_instance = SingleTable(self.__adj_mat.tolist(), 'router id = ' + str(self.__num + 1))
        table_instance.inner_heading_row_border = False
        table_instance.inner_row_border = True
        print(table_instance.table)

    def udp_send(self, msg, port, ip='127.0.0.1'):
        self.__send_sock.sendto(msg.encode(), (ip, port))

    def udp_send_neighbors(self, msg):
        for (port, neighbor) in zip(self.__PORTS, self.__neighborhood):
            if neighbor:
                self.udp_send(msg=msg, port=port)

    def udp_send_neighbors_poison(self):
        for (r, port, neighbor) in zip(range(1, len(self.__PORTS)+1), self.__PORTS, self.__neighborhood):
            if neighbor:
                advertisement_msg = self.__adj_mat[self.__num, :].copy()
                for idx in [i for i, x in enumerate(self.__routTable) if x == r]:
                    advertisement_msg[idx] = np.inf
                msg = str(advertisement_msg.tolist()) + "-" + str(self.__num + 1)
                self.udp_send(msg=msg, port=port)

    def __udp_rcv(self):
        while True:
            data, addr = self.__rcv_sock.recvfrom(1024)  # buffer size is 1024 bytes
            rlist, rnum = data.decode().split('-')
            rdata = str(rlist[1:len(rlist)-1]).split(',')
            i=0
            row = list()
            for d in rdata:
                if 'inf' not in d:
                    row.append(int(d.split('.')[0]))
                else:
                    row.append(np.inf)
                if row[i] != self.__adj_mat[int(rnum)-1,i]:
                    self.__cost_updated = True
                i = i+1
            self.__adj_mat[int(rnum)-1, :] = row
            self.__update_table_notify()

            sleep(0.1)

#Windows.enable(auto_colors=True, reset_atexit=True)  # Does nothing if not on Windows.

def key_release_handler(key):
    if str(key) in {"'s'", "'S'"} and not key_release_handler.router_started:
        print("I'm Here!")
        rt.start_route()
        key_release_handler.router_started = True
    elif str(key) in {"'u'", "'U'"} and key_release_handler.router_started:
        rt.check_link_cost()
key_release_handler.router_started = False


rt = Router(portfile=sys.argv[1], costfile=sys.argv[2], rid=sys.argv[3])
print('Press "S" to start Bellman-Ford algorithm.')


with keyboard.Listener(on_release=key_release_handler) as listener:
    listener.join()

while True:
    sleep(2)




